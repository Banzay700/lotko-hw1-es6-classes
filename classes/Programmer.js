import { Employee } from './Employee.js'

export class Programmer extends Employee {
   _companyGrades = { junior: 'Junior', middle: 'Middle', senior: 'Senior' }

   constructor(name, age, salary, lang) {
      super(name, age, salary)
      this.lang = lang
   }

   set salary(value) {
      const programmerMinSalary = this.companyMinimalSalary * 3

      if (value < programmerMinSalary) {
         this._salary = programmerMinSalary
      } else {
         this._salary = value
      }
   }

   checkExperience() {
      const experience = this.lang.length
      const { junior, middle, senior } = this._companyGrades
      const { premium, specialConditions } = this._companySalaryPolicy

      if (experience > 5) {
         this._expertiseLevel = senior
         this._salary += premium
      } else if (experience < 3) {
         this._expertiseLevel = junior
         this._salary = specialConditions
      } else {
         this._expertiseLevel = middle
      }
   }
}
