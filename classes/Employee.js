export class Employee {
   _companySalaryPolicy = {
      premium: 2000,
      companyMinimalSalary: 700,
      specialConditions: 'WORKS FOR FOOD',
   }

   constructor(name, age, salary) {
      this.name = name
      this.age = age
      this.salary = salary
   }

   get salary() {
      return this._salary
   }

   set salary(value) {
      const { companyMinimalSalary } = this._companySalaryPolicy

      if (value > companyMinimalSalary) {
         this._salary = companyMinimalSalary
      } else {
         this._salary = value
      }
   }
}
