import { Programmer } from './classes/Programmer.js'

const devOleg = new Programmer('Oleg', 21, 1200, ['HTML', 'CSS'])
const devAdam = new Programmer('Adam', 28, 1800, ['HTML', 'CSS', 'Sass', 'JavaScript', 'TypeScript', 'React'])
const devErica = new Programmer('Erika', 24, 2900, ['HTML', 'CSS', 'Sass', 'JavaScript'])

devOleg.checkExperience()
devAdam.checkExperience()
devErica.checkExperience()

console.log(devOleg)
console.log(devErica)
console.log(devAdam)
